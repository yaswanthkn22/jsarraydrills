
function map(elements , cb){
    let result = [];
    if(elements === undefined || !Array.isArray(elements)){
        return result;
    }
    if(Array.isArray(elements)){
        for (let index = 0 ; index < elements.length; index++ ){
            result[index] = cb(elements[index],index,elements);
        }
    }
    return result
}

module.exports = map;