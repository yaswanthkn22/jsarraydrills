const arrays = require('../arrays.cjs');
const reduce = require('../reduce.cjs');


function cb (acc, curr){
    return acc + Math.pow(curr,3);

}

const result_reduce = reduce(arrays.items , cb , 0);

console.log(`sum of elements is ${result_reduce}`);