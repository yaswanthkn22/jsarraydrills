const arrays = require('../arrays.cjs');
const filter = require('../filter.cjs');

function cb (element){
    return element%2 === 0;
}

const result_filter = filter(arrays.items , cb);

console.log(result_filter);