
function filter (elements = [], cb ){
    let result=[]
    
    if(elements === undefined || !Array.isArray(elements)){
        return result;
    }
    if(Array.isArray(elements)){
        for(let index = 0 ; index < elements.length; index++){
            if(cb(elements[index],index,elements) === true){
                result.push(elements[index])
            }
        }
    }
    return result;
}

module.exports = filter;