
function find(elements = [] , cb , req){
    for (let index = 0; index < elements.length; index++){
        if(cb(elements[index],req)){
            return true;
        }
    }
    return false;
}

module.exports = find;