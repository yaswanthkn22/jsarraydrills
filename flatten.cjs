
function cb(element ,results,depth,curr_depth){
    
   
        if(Number.isInteger(element) && element !== undefined && depth > curr_depth){
            results.push(element);
            return;
        }
        curr_depth += 1;
        if(Array.isArray(element) && depth > curr_depth){
            // console.log(curr_depth);
           
            for(let index = 0 ; index < element.length; index++){
                
                
                cb(element[index], results,depth,curr_depth);
                
                
            }
           
        }else {
            if((Array.isArray(element) || Number.isInteger(element)) && element != undefined){
                results.push(element);
                return;
            }
        }
        
       
}
// [1,[2],[[3]],[[[4]]] ]
function flatten(elements = [] ,depth=1){
    depth = depth+1;
    let results = []
    if(!Array.isArray(elements) || elements === undefined || elements.length === 0 ){
        return [];
    }
    for(let index = 0 ; index < elements.length; index++){
        cb(elements[index],results,depth,0);
    }
    return results;
}
let arr = [ 1, [2], [[3]],[ [ [ 4 ] ,[[[5]]]] ],[ [ [ [5]] ] ]];
console.log(flatten(arr,3));


module.exports = flatten;