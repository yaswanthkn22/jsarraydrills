

function reduce(elements , cb ,acc){
    

    if(!Array.isArray(elements) || elements === undefined || elements.length === 0){
        return [];
    }

    if(Array.isArray(elements) && elements !== undefined ){
        for (let index = 0; index < elements.length; index++){
            if(acc === undefined){
                acc = elements[index];
                continue;
            }

            acc = cb(acc ,elements[index],index,elements);
        }
    }
    return acc;
}   


module.exports = reduce;